//
//  AppDelegate.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 10/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    return true
  }
}

