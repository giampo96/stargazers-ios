//
//  ViewController.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 10/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import UIKit

/// This View Controller is responsable of the Stargazer's research by repo and owner.
class MainViewController: UIViewController {
  
  // MARK: Outlets

  // Textfield which will contain owner's name.
  @IBOutlet weak var ownerTexField: UITextField!
  // Textfield which will contain repo's name.
  @IBOutlet weak var repoTextField: UITextField!
  // Action button.
  @IBOutlet weak var goButton: UIButton!
  // TableView containing the list of stargazers.
  @IBOutlet weak var stargazersTableView: UITableView!
  // Action loader.
  @IBOutlet weak var loader: UIActivityIndicatorView!
  
  // MARK: Properties
  
  // Stargazers URLSessionDataTask instance.
  private var stargazersTask: URLSessionDataTask!
  // Array of Stargazers objects.
  private var stargazers: [Stargazer] = [] {
    didSet {
      updateUI()
    }
  }
  
  // MARK: VC Lifecycle Methods
  
  override func viewDidLoad() {
    super.viewDidLoad()
    styleUI()
  }
}

// MARK: UI Styling

extension MainViewController {
  /// Styles the User Interface.
  private func styleUI() {
    self.navigationController?.navigationBar.shadowImage = UIImage()
    self.view.backgroundColor = .myGray
    
    stargazersTableView.tableFooterView = UIView()
    stargazersTableView.backgroundColor = .clear
    
    loader.color = .myRed
    goButton.backgroundColor = .myRed
    goButton.tintColor = .white
    goButton.layer.cornerRadius = goButton.frame.height / 2.0
        
    let nib = UINib(nibName: "StargazersTableViewCell", bundle: nil)
    stargazersTableView.register(nib, forCellReuseIdentifier: "stargazerCell")
  }
  
  /// Updates the User Interface when data changes.
  private func updateUI() {
    self.stargazersTableView.reloadData()
  }
}

// MARK: Business Logic

extension MainViewController {
  /// Loads stargazers for a specific repo.
  private func handleStargazerLoading() {
    stargazersTask?.cancel()
    
    self.loader.startAnimating()
    
    guard let repo = self.repoTextField.text else {
      return
    }
    
    guard let owner = self.ownerTexField.text else {
      return
    }
    
    stargazersTask = StargazersService().loadStargazers(
      fromRepo: repo,
      by: owner,
      completion: { result, error in
        self.loader.stopAnimating()
        
        guard error == nil else {
          self.handleError(error!)
          return
        }
        
        guard let result = result else {
          return
        }
        
        if result.isEmpty {
          self.handleError(.empty)
        } else {
          self.stargazers = result
        }
    })
  }
}

// MARK: Actions

extension MainViewController {
  /// Go button action.
  @IBAction func go(_ sender: UIButton) {
    handleStargazerLoading()
  }
}

// MARK: Error Handling

extension MainViewController {
  /// Shows an error alert.
  private func showErrorAlert(with message: String) {
    let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
    let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
    alert.addAction(action)
    present(alert, animated: true, completion: nil)
  }
  
  /// Decide which kind of error will be displayed.
  private func handleError(_ error: ServiceError) {
    switch error {
    case .noInternetConnection:
      showErrorAlert(with: error.localizedDescription)
    case .other:
      showErrorAlert(with: error.localizedDescription)
    case .empty:
      showErrorAlert(with: error.localizedDescription)
    case .custom(let message):
      showErrorAlert(with: message)
    }
  }
}

// MARK: TableView Data Source

extension MainViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return stargazers.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "stargazerCell", for: indexPath) as! StargazersTableViewCell
    cell.backgroundColor = .clear
    
    let stargazer = stargazers[indexPath.row]
    
    if let avatar = stargazer.avatar {
      cell.avatarImageView.loadImage(from: avatar)
    } else {
      cell.avatarImageView.image = nil
    }
    
    cell.nameLabel.text = stargazer.name

    return cell
  }
}
