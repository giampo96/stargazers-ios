//
//  Stargazer.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 10/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import Foundation

// JSON dictionary typealias.
typealias JSON = [String: Any]

// MARK: Stargazer object

struct Stargazer: Codable {
  let name: String
  let avatar: String?
}

// MARK: Parsing JSON Settings

extension Stargazer {
  
  // MARK: Initializer
  
  init?(json: JSON) {
    guard let name = json["login"] as? String else {
      return nil
    }
    
    self.name = name
    self.avatar = json["avatar_url"] as? String
  }
}
