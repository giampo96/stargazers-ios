//
//  StargazersTableViewCell.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 14/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import UIKit

class StargazersTableViewCell: UITableViewCell {

  // MARK: Outlets
  
  // ImageView displaying the avatar image.
  @IBOutlet weak var avatarImageView: UIImageView!
  // Label containing the name of the stargazer.
  @IBOutlet weak var nameLabel: UILabel!
  
  // MARK: Awake From Nib
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    avatarImageView.layer.cornerRadius = avatarImageView.frame.height / 2.0
  }
}
