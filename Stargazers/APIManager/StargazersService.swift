//
//  StargazerService.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 11/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import Foundation

/// Manages the Stargazers service.
final class StargazersService{
  
  // MARK: Properties
  
  // Instantiate the API base url.
  private let client = NetworkManager(baseUrl: "https://api.github.com")
  
  // MARK: Methods
  
  // Handles the response from URLSession.
  @discardableResult
  func loadStargazers(fromRepo repo: String,
                      by owner: String,
                      completion: @escaping([Stargazer]?, ServiceError?) -> ()) -> URLSessionDataTask? {
    
    let path = "/repos/\(owner)/\(repo)/stargazers"
    
    return client.load(path: path, method: .get) { (result, error) in
      
      let dictionaries = result as? [JSON]
      DispatchQueue.main.async {
        completion(dictionaries?.compactMap(Stargazer.init), error)
      }
    }
  }
}
