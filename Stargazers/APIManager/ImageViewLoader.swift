//
//  ImageViewLoader.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 15/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import UIKit

// MARK: ImageView loader

// Images cache collection.
let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
  /// Manage the download and cache system of images.
  func loadImage(from imgURL: String!) {
    let request = URLRequest(url: URL(string: imgURL)!)

    // Set initial image to nil so it doesn't use the image from a reused cell.
    image = nil

    // Checks if the image already exists in the cache.
    if let imageToCache = imageCache.object(forKey: imgURL! as NSString) {
        self.image = imageToCache
        return
    }

    // Starts the request to download the image.
    let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
      if error != nil {
          return
      }

      DispatchQueue.main.async {
          let imageToCache = UIImage(data: data!)
          // Adds the image downloaded to the cache.
          imageCache.setObject(imageToCache!, forKey: imgURL! as NSString)
          self.image = imageToCache
      }
    }
    task.resume()
  }
}
