//
//  NetworkManager.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 10/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import Foundation

/// All the possible request methods.
enum RequestMethod: String {
  case get = "GET"
  case post = "POST"
  case put = "PUT"
  case delete = "DELETE"
}

/// Manages API requests.
final class NetworkManager {
  
  // MARK: Properties
  
  // The baseUrl of the API call.
  private var baseUrl: String
  
  // MARK: Initializer
  
  init(baseUrl: String) {
    self.baseUrl = baseUrl
  }
  
  // MARK: Methods
  
  // Load request method.
  func load(path: String,
            method: RequestMethod,
            completion: @escaping (Any?, ServiceError?) -> ()
  ) -> URLSessionDataTask? {
    
    // Check internet availability.
    if !Reachability.isConnectedToNetwork() {
      completion(nil, ServiceError.noInternetConnection)
      return nil
    }
    
    // Creating the request.
    let request = URLRequest(baseUrl: baseUrl, path: path, method: method)
    
    // Starting the URLSession.
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
      
      var object: Any? = nil
      if let data = data {
        object = try? JSONSerialization.jsonObject(with: data, options: [])
      }
      
      if let httpResponse = response as? HTTPURLResponse, (200..<300) ~= httpResponse.statusCode {
        completion(object, nil)
      } else {
        let error = (object as? JSON).flatMap(ServiceError.init) ?? ServiceError.other
        completion(nil, error)
      }
    }
    task.resume()
    
    return task
  }
}

// MARK: Custom URL

extension URL {
  
  // MARK: Initializer
  
  /// Builds the url with the added components.
  init(baseUrl: String, path: String, method: RequestMethod) {
    var components = URLComponents(string: baseUrl)!
    components.path += path
    
    self = components.url!
  }
}

// MARK: Custom URLRequest

extension URLRequest {
  
  // MARK: Initializer
  
  /// URLRequest settings.
  init(baseUrl: String, path: String, method: RequestMethod) {
    let url = URL(baseUrl: baseUrl, path: path, method: method)
    self.init(url: url)
    httpMethod = method.rawValue
    setValue("application/json", forHTTPHeaderField: "Accept")
    setValue("application/json", forHTTPHeaderField: "Content-Type")
  }
}
