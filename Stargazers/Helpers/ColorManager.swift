//
//  ColorManager.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 15/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import UIKit

// MARK: ColorManager
// Contains every custom colors present in the app.

extension UIColor {
  static let myRed = UIColor(red:0.96, green:0.26, blue:0.21, alpha:1.0)
  static let myGray = UIColor(red:0.98, green:0.98, blue:0.98, alpha:1.0)
}
