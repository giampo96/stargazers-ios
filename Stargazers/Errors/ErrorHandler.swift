//
//  ErrorHandler.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 10/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import Foundation

// MARK: Custom Service Errors

enum ServiceError: Error {
  case noInternetConnection
  case custom(String)
  case empty
  case other
}

// MARK: Error Descriptions

extension ServiceError: LocalizedError {
  var errorDescription: String? {
    switch self {
    case .noInternetConnection:
      return "No internet connection"
    case .other:
      return "Something went wrong"
    case .empty:
      return "Empty result"
    case .custom(let message):
      return message
    }
  }
}

extension ServiceError {
  
  // MARK: Initializer
  
  // Checks possible errors from the response.
  init(json: JSON) {
    if let message = json["message"] as? String {
      self = .custom(message)
    } else {
      self = .other
    }
  }
}
