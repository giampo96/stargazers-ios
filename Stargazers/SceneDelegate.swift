//
//  SceneDelegate.swift
//  Stargazers
//
//  Created by Giampiero Salemme on 10/10/2019.
//  Copyright © 2019 Giampiero Salemme. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
  var window: UIWindow?
  
  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    if #available(iOS 13.0, *) {
        window?.overrideUserInterfaceStyle = .light
    }
  }
}

