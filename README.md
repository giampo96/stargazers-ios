#  Stargazers

This is an iOS app project for Subito's iOS Engineer position.  

### What does it do?

This app allows you to search for a specific repository of a given GitHub user and returns the list of users who have placed the mentioned repository in their favorites.
  
The app manages GitHub API calls using native iOS components, no dependency manager is used.

### How to test?
You can use the following combination of repo and owner to test the app:

repo: *mojombo* owner: *god*  
repo *TheAlgorithms* owner: *Java*  
repo *opencv* owner: *opencv*  

You can find other repos here:
<https://github.com/trending>

*Warning: the Reachability.swift file was imported from an external library.*
